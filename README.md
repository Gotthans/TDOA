# Improving TDOA Radar Performance in Jammed Areas through Neural Network-Based Signal Processing
This paper presents a method for estimating the position of a target under jammed conditions using the Time Difference of Arrival (TDOA) method. The algorithm utilizes a deep neural network to overcome the challenges posed by the jammed conditions. The simulations and results indicate that the presented method is more accurate and efficient than the traditional TDOA methods. The use of deep neural networks in this application opens up new possibilities for developing advanced signal-processing algorithms for positioning systems and other related fields.

## About scripts
For testing purposes, a TDOA simulator was created in Python. The main idea was to create a simple square region where one antenna is placed at each point and our goal is to detect a moving object located inside the matrix. Our simulator then generates a reflected signal from the moving object for each antenna node. The operation of the simulator was verified by correlating the signals from individual antenna nodes and calculating the position of the object.

The simulator that is being discussed is a system that simulates the behavior of a radio signal being received by multiple antennas. It consists of four receiving antennas that are distributed in space. The purpose of this simulator is to simulate the movement of a target in space, and how the radio signal that is reflected off of the target is affected by the environment as it is received by the antennas. The signal that is reflected off of the target is attenuated by the environment, and this attenuated signal is then recorded by the receiving antennas.

One of the key features of this simulator is the ability to account for the varying delays that occur as the signal is received by the different antennas. These delays can be caused by a variety of factors, such as the distance between the target and the antennas. The simulator is able to extract these delays by cross-correlations from the signals received by the antennas, and use them to calculate the position of the target. 

If the goal is to confuse a radar system, one way to achieve this is by using a jammer, which is a device that transmits signals specifically designed to disrupt or interfere with the operation of the radar. In our case, the jammer is a generator of Zadoff-Chu sequences, which are complex sequences with unit amplitude and specific phase shifts. These sequences are able to overload and confuse the correlation detector of the radar system, making it impossible to detect the target reliably. This is known as jamming the radar.

In our simulator, we placed the jammers randomly in the environment, to make it very difficult for the radar system to mitigate the jamming. This added an additional layer of realism to the simulation, as it mimics the unpredictable nature of jamming in real-world scenarios. The second task of our TDOA simulator was to generate an interference signal that would preclude the use of standard detection methods based on signal correlation. This was done to simulate the impact of jamming on the radar system, and to test the effectiveness of different jamming countermeasures.

![NN setup](/images/NN_Setup2.png)

## More details
More details about this paper here: [a link](TDOA___Neural_Networks_Paper.pdf)

# Getting started
Main script is uploaded on Google Colab here: [a link](https://colab.research.google.com/drive/1Z0BPMe-Qn-ITGPYlaxhUpc1L5rKrWQuw?usp=sharing)

## Dataset
In the radars the BPSK signals in the form of the Barker codes are widely used. In thuis paper we have used the quadrature phase shift keying (QPSK) modulation has been widely applied in communication systems. The QPSK modulated signals have the features of low error rate, strong anti-jamming ability, and low complexity, so it is widely used in communications. We asume that the QPSK signal is reflected from the target.

## Jamming
Zadoff Chu (ZC) sequences are complex sequences with unit amplitude and specific phase shifts, and are now widely used in modern cellular systems such as LTE and 5G NR. They have replaced previous types of spread spectrum sequences, such as PN and Walsh sequences, which were commonly used in 3G cellular systems (WCDMA and cdma2000) and IS-95. Unlike Walsh and PN codes, which are real and binary valued (usually ±1), ZC sequences have a different structure. ZC sequences have a number of remarkable and desirable properties such as the cyclic auto-correlation of a ZC sequence is optimal, hence such sequences can be used as jammers with purpose to overload the receivers. The simulation is generating a PN (Pseudo-Noise) sequence using a Linear Feedback Shift Register (LFSR) for jamming purposes.

## Structure of neural network model
![Neural network structure](/images/model_plot.png)

# Licence
Please don't forget to cite this paper in case you consider to reuse/modify this idea:
